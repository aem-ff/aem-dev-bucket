package com.techaspect.core.aemdevbucket.core;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

/**
 * Class for retrieving file history from GIT Inspired by
 * http://wiki.eclipse.org/JGit/User_Guide
 * http://stackoverflow.com/questions/1685228/how-to-cat-a-file-in-jgit
 * http://www.programcreek.com/java-api-examples/index.php?api=org.eclipse.jgit.treewalk.TreeWalk
 */
public class GitAccessTest {
	public static void main(String args[]){
		System.out.println("in main");
		File file = new File("E:/AEM/POC/Self-POC/repo/aem-dev-bucket");
		System.out.println("file object :"+file);
		try {
			Git git = Git.open(file);
			System.out.println("git :"+git);
			CredentialsProvider cp = new UsernamePasswordCredentialsProvider( "sri.manogna801@gmail.com", "manogna*");
			git.pull().setCredentialsProvider( cp ).call();
			System.out.println("cp :: "+cp);
			//git.branchCreate().setName("remotes/origin/newBranch").call();
			List<Ref> refs = git.branchList().call();
			for (Ref ref : refs) {
                System.out.println("Branch-Created: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
            }
			RevCommit commit = git.commit().setMessage("test commit from Java").call();
			PushCommand pushCommand = git.push();
		    pushCommand.setCredentialsProvider(new UsernamePasswordCredentialsProvider("sri.manogna801@gmail.com", "manogna*"));
		    // you can add more settings here if needed
		    pushCommand.call(); 
		    System.out.println(" git.status().call() ::"+ git.status().call());
		    Status status = git.status().call();
wqqqqa			Set<String> added = status.getAdded();
			for (String add : added) {
				System.out.println("Added: " + add);
			}
		    System.out.println("pushCommand :"+pushCommand.toString());
		} catch (IOException | GitAPIException e) {
			e.printStackTrace();
		}
	}
}
